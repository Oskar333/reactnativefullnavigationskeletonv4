import React, { Component } from 'react';
import { createAppContainer } from "react-navigation";
import { createMaterialTopTabNavigator } from "react-navigation-tabs";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from 'react-navigation-drawer';
import Home from './src/components/Home';
import Profile from './src/components/Profile';
import Settings from './src/components/Settings';

//STACKS
const HomeStack = createStackNavigator({
  Home: Home
},
{
  defaultNavigationOptions: {
    header: null
  },
  initialRouteName: 'Home'
});

const SettingsStack = createStackNavigator({
  Settings: Settings
},
{
  defaultNavigationOptions: {
    header: null
  },
  initialRouteName: 'Settings'
});

//TABS
const TabNavigator = createMaterialTopTabNavigator( 
  {
   Home: HomeStack,
   Settings: SettingsStack   
  },
 {
   tabBarPosition: 'bottom',
   initialRouteName: "Home",
   tabBarOptions: {
     tabStyle: {
       backgroundColor: '#FFF',
     },
     style: {
       backgroundColor: 'FFF',
     },
     showLabel: true,
     activeTintColor: '#000',
     inactiveTintColor: '#cccccc'
   },
 }
);

//DRAWER
const DrawerNavigator = createDrawerNavigator(
  {
    Home: TabNavigator
  },
  {
    contentComponent: Profile
  }
);

const AppContainer = createAppContainer(DrawerNavigator);
export default class App extends Component {
 render() {
   return <AppContainer />;
 }
}